FROM ubuntu:bionic

WORKDIR /var/local

# installing tinytex from Yihui Xie 
# https://yihui.org/tinytex/
# https://github.com/rstudio/tinytex
RUN apt-get update && apt-get install -y perl wget libfontconfig1 && \
    wget -qO- "https://yihui.name/gh/tinytex/tools/install-unx.sh" | sh && \
    apt-get clean

ENV PATH="${PATH}:/root/bin"

# add extra packages from CTAN as needed
RUN tlmgr install \
    extsizes moresize titlesec \
    fontawesome raleway fetamont

RUN fmtutil-sys --all
